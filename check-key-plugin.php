<?php


/**
 * Plugin Name: check key new
 * Plugin URI: https://gitlab.com/boigaming1411/check-key-plugin
 * Description: new plugin from Tuan-nguyen
 * Version: 1.7
 * Author: Tuan-nguyen
 */


 defined('ABSPATH') or die('hacker');

 
 require 'plugin-update-checker/plugin-update-checker.php';
 use YahnisElsts\PluginUpdateChecker\v5\PucFactory;
  $myUpdateChecker = PucFactory::buildUpdateChecker(
 	 'https://gitlab.com/boigaming1411/check-key-plugin',
 	 __FILE__, //Full path to the main plugin file or functions.php.
 	 'check-key-plugin'
  );

  //Set the branch that contains the stable release.
  $myUpdateChecker->setBranch('main');

  //Optional: If you're using a private repository, specify the access token like this:
  $myUpdateChecker->setAuthentication('glpat-Y6sXSWzcxmYAYmyxJRxR');






    $check_key_plugin = new check_key_new();

class check_key_new
{
	private $section_first = "check_key_first_section";
	private $page_setting = 'check-key-setting';
//	public $plugin_floder_path ;



    public function __construct() {
//		$this->plugin_floder_path = plugin_dir_url(__FILE__);

	    add_action('admin_menu', array($this, 'register_menu'));
	    add_action('admin_menu', array($this, 'register_sub_page'));

	    add_action('admin_init', array($this, 'settings'));

    }

	public function settings(  ) {
		add_settings_section($this->section_first,null,null,$this->page_setting);
		add_settings_field('check_key_data','Check key data',array($this,'screen_to_type_key'),$this->page_setting,$this->section_first);
		register_setting('check_key_plugin_group','check_key_value',array('sanitize_callback'=>'','default'=>''));
		register_setting('check_key_plugin_group','check_key_user_token',array('sanitize_callback'=>'','default'=>''));


	}

	public function register_sub_page(  ) {
		add_submenu_page('check-key-setting','check-key-user-use','user interactive','manage_options','check-key-user-use',array($this,'check_key_page_user_use'));
	}
	



	public function register_menu() {
        add_menu_page('check-key-setting-page','check-key','manage_options','check-key-setting',array($this,'check_key_page_setting'),'dashicons-admin-site-alt',120);
    }

	public function check_key_page_setting(  ) {
		include_once plugin_dir_path(__FILE__).'check_key_setting_page.php';
    }

	public function check_key_page_user_use() {
		include_once plugin_dir_path(__FILE__).'templates\check_key_page_user_use.php';
	}



}





?>