<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<style>
    :root {
        --loading-spinner-background: rgba(200, 200, 200, 0.20);
        --loading-spinner-body: blue;
        --screen-height : 100vh;
    }
    body {
        background: #fff;
    }
    .wrapper {
        height: var(--screen-height);
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .loading-spinner, .loading-spinner::after {
        width: 25px;
        height: 25px;
        border-radius: 50%;
    }

    .loading-spinner {
        border-top: 3px solid var(--loading-spinner-background);
        border-right: 3px solid var(--loading-spinner-background);
        border-bottom: 3px solid var(--loading-spinner-background);
        border-left: 3px solid var(--loading-spinner-body);
        position: relative;
        -webkit-transform: translateZ(0);
        transform: translateZ(0);
        -webkit-animation-name: loading-spinner-animation;
        animation-name: loading-spinner-animation;
        -webkit-animation-iteration-count: infinite;
        animation-iteration-count: infinite;
        -webkit-animation-duration: 0.8s;
        animation-duration: 0.8s;
        -webkit-animation-timing-function: linear;
        animation-timing-function: linear;
    }

    @-webkit-keyframes loading-spinner-animation {
        0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
        }
        100% {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @keyframes tw-loading-spinner-animation {
        0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg); }
        100% {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

/*    Wrap content below*/

    .load-user-iframe{
        display: none ;

    }

    .load-user-iframe{
        height: var(--screen-height);
        width: 100%;


    }

    .user-iframe{
        width: 100%;
        height: 100%;
    }




</style>

<!--Loading Effect-->




<!-- Load User iframe -->
<div class="wrap">
    <div class="wrapper">
        <div class="loading-spinner"></div>
    </div>

    <div class="load-user-iframe">
        <iframe class="user-iframe" ></iframe>
    </div>
</div>



<script>
    window.localStorage.setItem('user_token','N3WK8H3RQtSLyHrmhlweE4fM5PXG7amAlIK9jeRN');


    const token = window.localStorage.getItem('user_token');

    async function getSrc() {
        const res = await fetch("http://127.0.0.1:8000/api/user_info", {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer '+token
            }
        });
        const blob = await res.blob();
        const urlObject = URL.createObjectURL(blob);
        // $(".wrapper").hide();
        document.querySelector('.wrapper').style.display = "none";
        document.querySelector("body").style.background = '#fff';
        // $(".wrap").css('display','block !important');
        document.querySelector('.load-user-iframe').style.display = "block";
        document.querySelector('iframe').setAttribute("src", urlObject)
    }
    getSrc();





</script>

</body>
</html>